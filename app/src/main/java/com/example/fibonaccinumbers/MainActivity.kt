package com.example.fibonaccinumbers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.fibonaccinumbers.databinding.ActivityMainBinding
import kotlinx.coroutines.Job

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var textView : TextView
    private lateinit var editText : EditText
    private var calculationRunning : Boolean = false
    private var coroutine : Job? = null

    private fun init() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        textView = binding.resultTextView
        editText = binding.numberEditText
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()
        setContentView(binding.root)

        savedInstanceState?.apply {
            var result = getString("Result")
            if (result != null) textView.text = result

            if (savedInstanceState.getBoolean("Calculation running")) {
                setDisabled()
            }
        }

        val viewModel = ViewModelProvider(this)[FibonacciViewModel::class.java]
        viewModel.result.observe(this) { result ->
            if (calculationRunning) textView.text = result
            if (result.contains("Result")) {
                setEnabled()
            }
        }
        coroutine = viewModel.coroutine

        binding.button.setOnClickListener {
            if (binding.button.text == "Start") {
                viewModel.count(binding.numberEditText.text.toString().toInt())
                coroutine = viewModel.coroutine
                setDisabled()
            } else {
                coroutine!!.cancel()
                textView.text = ""
                setEnabled()
            }
        }
    }

    private fun setEnabled() {
        binding.button.text = getString(R.string.start)
        editText.isEnabled = true
        calculationRunning = false
    }

    private fun setDisabled() {
        binding.button.text = getString(R.string.cancel)
        editText.isEnabled = false
        calculationRunning = true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        var result = textView.text.toString()
        if (result.contains("Result")) outState.putString("Result", result)
        outState.putBoolean("Calculation running", calculationRunning)
    }
}