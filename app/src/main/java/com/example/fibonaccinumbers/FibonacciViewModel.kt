package com.example.fibonaccinumbers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FibonacciViewModel : ViewModel(){
    private val _result = MutableLiveData<String>()
    var coroutine : Job? = null
    val result: LiveData<String>
        get() = _result


    fun count(number : Int)  {
        coroutine = CoroutineScope(Dispatchers.Default).launch {
            var t1 = 0
            var t2 = 1
            var sum = 0

            for (i in 0..number) {
                if (!coroutine!!.isActive) {
                    return@launch
                } else {
                    sum = t1 + t2
                    t1 = t2
                    t2 = sum

                    _result.postValue("Current is $sum")

                    try {
                        delay(1000)
                    } catch (e : CancellationException) {
                        coroutine!!.cancel()
                    }
                }
            }

            _result.postValue("Result is $sum")
        }
    }
}